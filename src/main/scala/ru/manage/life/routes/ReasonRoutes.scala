package ru.manage.life.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ru.manage.life.dao.ReasonDAO
import ru.manage.life.entity.Reason

import scala.concurrent.Future
import scala.util.{Failure, Success}

class ReasonRoutes(reasonService: ReasonDAO) {

  val routes: Route = {
      pathPrefix("reason"){
      path("list") {
        get {
          onSuccess(Future.successful(reasonService.list())) { reasons =>
            complete(reasons.mkString("reasons: ", ", ", ""))
          }
        }
      } ~
        post { //insert
          entity(as[Reason]){ newReason =>
            onSuccess(Future.successful(reasonService.insert(newReason))) { savedReason =>
              complete(StatusCodes.Created, savedReason.toString)
            }
          }
        } ~
        put { //update
          entity(as[Reason]){ updateReason =>
            onSuccess(Future.successful(reasonService.update(updateReason))) { updatedReason =>
              complete(StatusCodes.Created, updatedReason.toString)
            }
          }
        } ~
        get { //get by id
          path(Segment) { id =>
            rejectEmptyResponse {
              reasonService.select(id) match {
                case None => complete(StatusCodes.NotFound, "fuck")
                case Some(reason) => complete(StatusCodes.OK, reason.toString)
              }
            }
          }
        } ~
        delete {
          path(Segment) { id =>
            rejectEmptyResponse {
              onComplete(Future.successful(reasonService.delete(id))) {
                case Success(_) => complete(StatusCodes.OK, "Complete Delete")
                case Failure(ex) => complete(StatusCodes.NotFound, ex)
              }
            }
          }
        }
    }
  }
}
