package ru.manage.life.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ru.manage.life.dao.InjectionDAO
import ru.manage.life.entity.Injection
import spray.json.{JsValue, JsonReader}

import scala.concurrent.Future
import scala.util.{Failure, Success}

class InjectionRoutes(injectionService: InjectionDAO) {

  val routes: Route = {
    pathPrefix("injection"){
      path("list") {
        get {
          onSuccess(Future.successful(injectionService.list())) { injections =>
            complete(injections.mkString("injections: ",", " ,""))
          }
        }
      } ~
        post { //insert
          entity(as[JsValue]){ newInjection =>
            onSuccess(Future.successful(newInjection.convertTo[Injection](ru.manage.life.entity.InjectionJson.jsonInjectionReader))) { savedInjection =>
              complete(StatusCodes.Created, savedInjection.toString)
            }
          }
        } ~
        put { //update
          entity(as[String]){ updateInjection =>
            onSuccess(Future.successful(updateInjection)) { updatedInjection =>
              complete(StatusCodes.Created, updatedInjection)
            }
          }
        } ~
        get { //get by id
          path(Segment) { id =>
            rejectEmptyResponse {
              injectionService.select(id) match {
                case None => complete(StatusCodes.NotFound, "fuck")
                case Some(injection) => complete(StatusCodes.OK, injection.toString)
              }
            }
          }
        } ~
        delete {
          path(Segment) { id =>
            rejectEmptyResponse {
              onComplete(Future.successful(injectionService.delete(id))) {
                case Success(_) => complete(StatusCodes.OK, "Complete Delete")
                case Failure(ex) => complete(StatusCodes.NotFound, ex)
              }
            }
          }
        }
    }
  }
}
