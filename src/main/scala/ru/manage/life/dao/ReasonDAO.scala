package ru.manage.life.dao

import ru.manage.life.entity.Reason
import io.getquill.{idiom => _}
import ru.manage.life.configuration.TransactorDB.ctx._

import scala.util.Success

trait ReasonDAO {
  def insert(reason: Reason): Reason
  def delete(id: String): Unit
  def update(reason: Reason): Reason
  def select(id: String): Option[Reason]
  def list(): Seq[Reason]
}

class ReasonDAOImpl extends ReasonDAO {

  private val reason = quote(querySchema[Reason]("Reason"))

  override def insert(entity: Reason): Reason = {
    val result = runIO(reason.insert(lift(entity))).flatMap{ _ =>
      runIO(reason.filter(_.id == lift(entity.id)))
    }
    performIO(result).head
  }

  override def delete(id: String): Unit = {
    run(reason.filter(_.id == lift(id)).delete)
  }

  override def update(entity: Reason): Reason = {
    val result = runIO(reason.update(lift(entity))).flatMap{ _ =>
      runIO(reason.filter(_.id == lift(entity.id)))
    }
    performIO(result).head
  }

  override def select(id: String): Option[Reason] = {
    run(reason.filter(c => c.id == lift(id))).headOption
  }

  def list(): Seq[Reason] = {
    run(reason)
  }

}