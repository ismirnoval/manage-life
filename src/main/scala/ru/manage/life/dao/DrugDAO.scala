package ru.manage.life.dao

import ru.manage.life.entity.Drug
import ru.manage.life.configuration.TransactorDB.ctx._
import io.getquill.{idiom => _}



trait DrugDAO {
  def insert(drug: Drug): Drug
  def delete(id: String): Unit
  def update(drug: Drug): Drug
  def select(id: String): Option[Drug]
  def list(): Seq[Drug]
}

class DrugDAOImpl extends DrugDAO {

  private val drug = quote(querySchema[Drug]("Drug"))

  override def insert(entity: Drug): Drug = {
    val result = runIO(drug.insert(lift(entity))).flatMap{ _ =>
      runIO(drug.filter(_.id == lift(entity.id)))
    }
    performIO(result).head
  }

  override def delete(id: String): Unit = {
    run(drug.filter(_.id == lift(id)).delete)
  }

  override def update(entity: Drug): Drug = {
    val result = runIO(drug.update(lift(entity))).flatMap{ _ =>
      runIO(drug.filter(_.id == lift(entity.id)))
    }
    performIO(result).head
  }

  override def select(id: String): Option[Drug] = {
    run(drug.filter(c => c.id == lift(id))).headOption
  }

  def list(): Seq[Drug] = {
    run(drug)
  }

}
