package ru.manage.life.entity

import java.sql.Timestamp

import spray.json.JsonReader

case class Injection(id: String,
                    drugId: String,
                    reasonId: String,
                    dateOfInjection: Timestamp,
                    number: Int)

object InjectionJson {
  implicit val jsonInjectionReader = JsonReader[Injection]
}