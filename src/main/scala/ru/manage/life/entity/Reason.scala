package ru.manage.life.entity

import spray.json.DefaultJsonProtocol

case class Reason(id: String, reasonText: String)

object ReasonJson extends DefaultJsonProtocol {
  implicit val reasonFormat = jsonFormat2(Reason)
}