package ru.manage.life.configuration
import com.typesafe.config.ConfigFactory
import pureconfig._
import pureconfig.generic.auto._ // not remove

private case class TestConfig(gateway: Gateway, database: Database)

private case class Gateway(host: String, port: Int)
//trait DBConnection
private case class Database(url: String, user: String, password: String)

object Config {
  private val configSource = ConfigSource.fromConfig(ConfigFactory.defaultApplication).loadOrThrow[TestConfig]

  lazy val host: String = configSource.gateway.host
  lazy val port: Int = configSource.gateway.port

  lazy val dbUrl: String = configSource.database.url
  lazy val dbUser: String = configSource.database.user
  lazy val dbPassword: String = configSource.database.password

}