
import scala.util.{Success, Failure, Try}
////
////
//import scala.annotation.tailrec
//////import scala.annotation.tailrec
//////import scala.annotation.tailrec
//////
//////def longestSubarray(nums: Array[Int], limit: Int): Int = {
//////  var count = 0
//////  @tailrec
//////  def repeat(seq: Seq[Int]): Seq[Seq[Int]] = {
//////    def repeatGo(seq: Seq[Int]) = {
//////      val result = seq.foldLeft[Seq[Seq[Int]]](Seq.empty[Seq[Int]]){ (acc, el) =>
//////        acc :+ (if(acc.nonEmpty)(acc.last :+ el) else Seq(el))
//////      }
//////      check(result)
//////    }
//////    if(seq.isEmpty){
//////      Seq.empty[Seq[Int]]
//////    } else {
//////      repeatGo(seq)
//////      repeat(seq.tail)
//////    }
//////  }
//////
//////  def check(arrays: Seq[Seq[Int]]) = {
//////    val arr = arrays.sortBy(_.size)
//////    var i = 0
//////    while (i < arr.length - 1 && !(arr(i).length > count && Math.abs(arr(i).min - arr(i).max) <= limit)){
//////      i = i + 1
//////    }
//////    count = arr(i).length
////////    arrays.sortBy(_.length).foreach { array =>
////////      if(array.length > count && Math.abs(array.min - array.max) <= limit) {
////////        count = array.length
////////      }
////////    }
//////  }
//////
//////  def go(seq: Array[Int]): Int = {
//////    if(seq.isEmpty) {
//////      count
//////    } else {
//////      repeat(seq)
//////      count
//////    }
//////  }
//////  go(nums)
//////}
//////
//////longestSubarray(Array(4293,7527,8628,2663,2272,2263,7221,5917,5076,6566,3941,1898,3379,5006,1118,1744,3730,6505,5266,8067,1638,3060,737,907,9790,382,3549,569,8102,8660,7247,5611,4499,1935,2830,1561,3639,6549,3822,8111,7370,6192,2763,3797,3863,4648,7579,8963,9502,6384,3472,7401,4226,5409,8132,9726,1818,3504,1292,758,253,86,7871,5236,6278,9507,4706,3210,6553,2623,2495,500,249,6346,2379,8890,936,9253,6218,4165,2130,1303,7584,4971,237,582,8798,997,8601,2113,702,4483,1925,7087,1492,3115,453,8187,2136,4576,3145,7333,6385,5404,4391,4258,3983,4022,987,9633,9675,2872,5729,7021,9919,9180,8556,997,5788,8961,6761,8817,3267,8789,6949,1149,4377,1185,4122,2910,1583,2475,962,3332,7015,736,866,1495,8403,7687,8211,6067,1627,5124,658,2075,8708,544,7264,2101,6475,7309,404,8594,4424,1481,4097,5332,1406,4946,561,6296,953,4277,5132,2131,4265,6229,1920,4977,1542,6961,4021,8237,9129,3366,5409,5549,8345,6408,9571,7884,1716,2126,7351,8582,9153,9526,8521,8776,496,4774,2573,3277,6068,6378,8538,5313,1596,6710,5659,2071,9420,1063,714,4924,8744,5139,6842,4888,5224,5778,4468,5330,8522,8210,142,8621,1997,2438,5196,5335,5367,9390,1128,7403,4582,7860,7441,5967,6235,1281,9486,920,2205,799,8581,8064,9431,4128,4021,9401,5549,5925,6065,6596,5037,7612,9802,5578,8719), 9966)
//////
//////
////////def repeatGo(list: Seq[Int]): Seq[Seq[Int]] = {
////////  list.foldLeft[Seq[Seq[Int]]](Seq.empty[Seq[Int]]){ (acc, el) =>
////////    acc :+ (if(acc.nonEmpty) (acc.last :+ el) else Seq(el))
////////  }
////////}
////////def repeat(seq: Seq[Int]): Seq[Seq[Int]] = {
////////  seq match {
////////    case Nil => {
////////      Seq.empty[Seq[Int]]
////////    }
////////    case elements => {
////////      repeatGo(elements) ++ repeat(elements.tail)
////////    }
////////  }
////////}
////////repeatGo(Seq(4,2))
////////repeat(Seq(4,2))
//////
////////def check(arrays: Set[Seq[Int]]) = {
////////  arrays.map { array =>
////////    if (array.nonEmpty) {
////////      val s = for {
////////        el1 <- array
////////        el2 <- array
////////      } yield Math.abs(el1 - el2)
////////      if ((s.min <= limit) && (s.max <= limit)) {
////////        count = if (array.length >= count) array.length else count
////////      }
////////    } else {
////////      ()
////////    }
////////  }
////////}
////
////
////
////
////
////
////
////
////
////
////////kthSmallest
//////def kthSmallest(mat: Array[Array[Int]], k: Int): Int = {
//////  val m = mat.length
//////  var max = 0;
//////  @tailrec
//////  def go(arr: Seq[Seq[Int]]): Seq[Seq[Int]] = {
//////    val (seq2s, seq) = arr.grouped(2).partition(_.size == 2)
//////    val seqs = if(seq.isEmpty){
//////      val seq2s_part1 = seq2s.flatMap { seq2 =>
//////        zip2Array(seq2(0), seq2(1))
//////      }.toSeq
//////      max = seq2s_part1.map(_.sum).sorted.take(k).max
//////      seq2s_part1
//////    } else {
//////      val seq2s_part1 = seq2s.flatMap { seq2 =>
//////        zip2Array(seq2(0), seq2(1))
//////      }.toSeq
//////      val seq2s_part2 = zipElWithSeq(seq.toSeq(0)(0), seq2s_part1)
//////      max = seq2s_part2.map(_.sum).sorted.take(k).max
//////      seq2s_part2
//////    }
//////    println(seqs(0).length)
//////    if(seqs(0).length < m) go(seqs) else seqs
//////  }
//////  def zipElWithSeq(left: Seq[Int], seq2: Seq[Seq[Int]]): Seq[Seq[Int]] = {
//////    left.flatMap { el =>
//////      seq2.map { seq =>
//////        seq :+ el
//////      }
//////    }
//////  }
//////
//////  def zip2Array(left: Seq[Int], right: Seq[Int]): Seq[Seq[Int]] = {
//////    left.flatMap { el =>
//////      right.foldLeft[Seq[Seq[Int]]](Seq.empty[Seq[Int]]) { (acc, element) =>
//////        acc :+ Seq(el, element)
//////      }
//////    }
//////  }
//////  go(mat map (_.toSeq)).map(_.sum).sorted.take(k).max
////////  val arr = mat
////////  val (seq2s, seq) = arr.grouped(2).partition(_.size == 2)
////////
////////  val seq2s_part1 = seq2s.flatMap { seq2 =>
////////    zip2Array(seq2(0), seq2(1))
////////  }.toSeq
////////
////////
////////  println(seq2s.map(_.mkString(", ")).mkString("; "))
////////  if(seq.isEmpty){
////////    seq2s_part1.map(_.sum).sorted.take(k).max
////////  } else {
////////    val seq2s_part2 = zipElWithSeq(seq.toSeq(0)(0), seq2s_part1)
////////    seq2s_part2.map(_.sum).sorted.take(k).max
////////  }
//////
//////}
//////
//////kthSmallest(Array(Array(1,3,11),Array(2,4,6)), 5)
//////
//////kthSmallest(Array(Array(1,3,11),Array(2,4,6)), 9)
//////
//////kthSmallest(Array(Array(1,10,10), Array(1,4,5), Array(2, 3, 6)), 7)
//////
//////kthSmallest(Array(Array(1,1,10), Array(2,9,9)), 7)
//////
//////
//////
//////kthSmallest(Array(Array(1,2,7,8,10), Array(4,4,5,5,6), Array(3,3,5,6,7), Array(2,4,7,9,9)), 7)
////
//////
//////def maxPower(s: String): Int = {
//////  s.
//////}
//////def maxPower(s: String): Int = {
//////  var useChar = ""
//////  var max = 0
//////  s.toSeq.map(_.toString).foldLeft[Seq[(String, Int)]](Seq.empty[(String, Int)]) { (acc, el) =>
//////    if (useChar == el) {
//////      max = max + 1
//////      useChar = el
//////    } else {
//////      max = 1
//////      useChar = el
//////    }
//////    acc :+ (el, max)
//////  }.sortBy(_._2).toList.head._2
//////}
//////
//////
//////
//////def simplifiedFractions(n: Int): List[String] = {
//////  if(n == 1)  List.empty[String]
//////  else {
//////    val elements = 1 to n
//////    elements.flatMap{ up =>
//////      elements.filter(down => down > up && !elements.tail.exists(el => (up % el) == 0 && (down % el) == 0)).map{ down =>
//////        s"${up}/${down}"
//////      }
//////    }.toList
//////
//////  }
//////}
//////
//////simplifiedFractions(6)
//////
////
////
////
////class TreeNode(_value: Int = 0, _left: TreeNode = null, _right: TreeNode = null) {
////  var value: Int = _value
////  var left: TreeNode = _left
////  var right: TreeNode = _right
////}
////////object Solution {
////  def goodNodes(root: TreeNode): Int = {
////    var count = 0;
////    @tailrec
////    def getLeft(value: Int, node: TreeNode): Unit = {
////      if(node != null) {
////        if (node.value >= value) {
////          count = count + 1
////        }
////        if (node.right != null) {
////          getRight(value, node.right)
////        }
////        if (node.left != null) {
////          getLeft(value, node.left)
////        }
////      }
////    }
////    @tailrec
////    def getRight(value: Int, node: TreeNode): Unit = {
////      if(node != null){
////        if(node.value >= value) {
////          count = count + 1
////        }
////        if(node.left != null){
////          getLeft(value, node.left)
////        }
////        if(node.right != null){
////          getRight(value, node.left)
////        }
////      }
////    }
////    getLeft(root.value, root.left)
////    getRight(root.value, root.right)
////    count
////  }
////  goodNodes(new TreeNode(3, new TreeNode(3, new TreeNode(4), new TreeNode(2))))
////}
////
////
////def largestNumber(cost: Array[Int], target: Int): String = {
////  val costWithNum = cost.zipWithIndex.sortBy(_._1)
////
////  def go(costWithNums: Array[(Int, Int)]): Unit ={
////    costWithNums match {
////      case
////        ()-}
////  }
////
////}
////Int.MinValue
////Int.MaxValue
////
////Math.round(0.00001)
////
////def myPow(x: Double, n: Int): Double = {
////  def pow(xx: Double, nn: Int): Double = {
////    nn % 2 match {
////      case 0 if nn >= 2 => pow(xx, nn/2) * pow(xx, nn/2)
////      case 1 if (nn > 2) => xx * pow(xx, nn/2) * pow(xx, nn/2)
////      case 1 if (nn < 2) => xx
////    }
////  }
////  (x, n) match {
////    case (0, _) => 0
////    case (1, _) => 1
////    case (_, 0) => 1
////    case (_, n) if n > 0 => pow(x, n)
////    case (_, n) if n < 0 => 1 / pow(x, (-1 * n))
////  }
//////}
////myPow(0.00001, 2147483647)
////myPow(2.10000, 3)
////myPow(2.00000, -2147483647)


class ListNode(_x: Int = 0, _next: ListNode = null) {
    var next: ListNode = _next
    var x: Int = _x
}
def swap(el: ListNode, el2: ListNode): ListNode = {
  new ListNode(el2.x, el)
}
//object Solution {
//def reverse(el: ListNode): ListNode = {
//  if(el == null) null
//  else if(el.next == null) {
//
//  }
//}


//def printList(list: ListNode): Unit = {
//  var x = list
//  while(x != null) {
//    println(x.x)
//    x = x.next
//  }
//}
//val myList = new ListNode(1, new ListNode(4, new ListNode(0, new ListNode(5))))
////printList(myList)
////val result = reverse(myList)
//
////}


//ListToInt(myList)
//
//def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {
//  def ListNodeTo(el: ListNode): BigInt = {
//    var now = el
//    var string: String = ""
//    while (now != null){
//      string = string.concat(now.x.toString)
//      now = now.next
//    }
//    BigInt(string.reverse)
//  }
//  def StringTo(numb: String): ListNode = {
//    val result = new ListNode()
//    var link = result
//    val iterator = numb.iterator
//    iterator.foreach{ el =>
//      link.x = el.toString.toInt
//      if(iterator.hasNext) {
//        link.next = new ListNode()
//        link = link.next
//      }
//    }
//    result
//  }
//  StringTo((ListNodeTo(l1) + ListNodeTo(l2)).toString.reverse)
//}

//printList(addTwoNumbers(myList, myList))


//
//def isPrefixOfWord(sentence: String, searchWord: String): Int = {
//  var result = -1
//  sentence.split(" ").zipWithIndex.foreach{ case (word, ind) =>
//    if(word.startsWith(searchWord) && word.size > searchWord.size && result == -1) {
//      result = ind + 1
//    }
//  }
//  result
//}


//def maxVowels(s: String, k: Int): Int = {
//  val vowels = Seq("a", "e", "i", "o", "u")
//  if(s.exists(el => vowels.contains(el.toString))) {
//    s.tails.take(k).flatMap(_.grouped(k)).filter(_.length == k).filter(el => vowels.map(char => el.contains(char)).reduce(_||_)).map{ se =>
//      se.count(char => vowels.contains(char.toString))
//    }.max
//  } else 0
//}
////val vowels = Seq("a", "e", "i", "o", "u")
////"leetcode".tails.take(3).flatMap(_.grouped(3)).filter(_.size == 3).filter( el => vowels.map(char => el.contains(char)).reduce(_||_)).toList.map{ se =>
////  se.count(char => vowels.contains(char.toString))
////
////}.toList
//
//maxVowels("rhythms", 4)



def maxDotProduct(nums1: Array[Int], nums2: Array[Int]): Int = {
  def TryGet(arr: Array[Int], index: Int):Int = {
    Try(arr(index)) match {
      case Success(el) => el
      case Failure(ex) => 0
    }
  }
  val res = for{
    i <- 0 to nums1.length
    j <- 0 to nums2.length
    ii <- i+1 to nums1.length
    jj <- j+1 to nums2.length
  } yield (TryGet(nums1,i) * TryGet(nums2,j)) + (TryGet(nums1,ii) * TryGet(nums2,jj))
//  (i,j, ii, jj)
  println(res.mkString(", "))
  println(res.max)
  res.max
  1
}
///
//maxDotProduct(Array(2,1,-2, 5), Array(3,0,-6)) //18
//maxDotProduct(Array(3,-2,0), Array(2,-6,7)) //21
//maxDotProduct(Array(-1,-1), Array(1,1)) //-1
maxDotProduct(Array(-5,3,-5,-3,1), Array(-2,4,2,5,-5)) //50


val i = Array(-5,3,-5,-3,1).indices
val j = Array(-2,4,2,5, -5).indices

//(0 to((i.length + j.length), 2))
val res = (0 to((i.length + j.length), 2)).map { count =>
  (i.toList ++ j.toList).combinations(count).map(el => println(el.mkString(", "))).mkString("; ")
  (i.toList).combinations(count).map{ el =>
    println(s"el: ${el.mkString(", ")}")
    el.grouped(2).map{ arr =>
      println(s"arr: ${arr.mkString(", ")}")
      (Array(-5,3,-5,-3,1)(arr.head), Array(-2,4,2,5, -5)(arr(1)))
    }
  }
}

res.map(_.map(_.mkString(", ")).mkString(" ")).mkString(";\n ")
//res..sorted

//0 1
//3 2
//
//0 3
//0 2
//1 3
//1 2
//
//0 1 2
//5 4 3
//
//0 5
//0 4
//0 3
//1 5
//1 4
//1 3
//2 5
//2 4
//2 3
//0 5 1 4
//0 5 1 3
//0 5 2 4
//0 5 2 3
//1 5 2 4
//1 5 2 3
