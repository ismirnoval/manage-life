create table `Reason` (
    id VARCHAR(32) PRIMARY KEY NOT NULL,
    reasonText TEXT NOT NULL
);
create table Injection (
    id VARCHAR(32) PRIMARY KEY NOT NULL,
    drugId VARCHAR(32) NOT NULL,
    reasonId VARCHAR(32) NOT NULL,
    dateOfInjection TIMESTAMP NOT NULL,
    number INT NOT NULL
);
create table Drug (
    id VARCHAR(32) PRIMARY KEY NOT NULL,
    barCode VARCHAR (32) NOT NULL,
    name TEXT NOT NULL,
    systemEntryDate TIMESTAMP NOT NULL,
    expiryDate TIMESTAMP NOT NULL,
    dosage INT NOT NULL,
    number INT NOT NULL
);

insert into `Reason` (id, reasonText) values ('124213', 'fqmwkldmeqw');
insert into `Reason` (id, reasonText) values ('124214', '214');
insert into `Reason` (id, reasonText) values ('124215', '4554');