lazy val akkaHttpVersion = "10.1.11"
lazy val akkaVersion    = "2.6.4"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      version := "0.1-SNAPSHOT",
      organization    := "ru.manage.life",
      scalaVersion    := "2.12.1"
    )),
    name := "life-manager",
    libraryDependencies ++= Seq(
      "com.typesafe.akka"         %%  "akka-http"                % akkaHttpVersion,
      "com.typesafe.akka"         %%  "akka-http-spray-json"     % akkaHttpVersion,
      "com.typesafe.akka"         %%  "akka-actor-typed"         % akkaVersion,
      "com.typesafe.akka"         %%  "akka-stream"              % akkaVersion,
      "ch.qos.logback"            %   "logback-classic"          % "1.2.3",

      "org.flywaydb"              %   "flyway-core"              % "6.4.0",

      "com.github.pureconfig"     %%  "pureconfig"               % "0.12.3",

      // doobie
      "org.tpolecat"              %%  "doobie-core"              % "0.8.8",
      "org.tpolecat"              %%  "doobie-postgres"          % "0.8.8",                   // Postgres driver 42.2.9 + type mappings.
      "org.tpolecat"              %%  "doobie-quill"             % "0.8.8",                   // Support for Quill 3.4.10

      "org.tpolecat"              %%  "doobie-h2"                % "0.8.8",

      "com.h2database"            %   "h2"                       % "1.4.199",
      "org.postgresql"            %   "postgresql"               % "42.2.8",

      "io.getquill"               %%  "quill-jdbc"               % "3.5.1",

//      "org.apache.spark" %% "spark-core" % "2.4.5" % "provided",
//      "org.apache.spark" %% "spark-sql" % "2.4.5" % Provided,

      "org.tpolecat"              %%  "doobie-specs2"            % "0.8.8"          % Test,   // Specs2 support for typechecking statements.
      "org.tpolecat"              %%  "doobie-scalatest"         % "0.8.8"          % Test,   // ScalaTest support for typechecking statements.

      "com.typesafe.akka"         %%  "akka-http-testkit"        % akkaHttpVersion  % Test,
      "com.typesafe.akka"         %%  "akka-actor-testkit-typed" % akkaVersion      % Test,
      "org.scalatest"             %%  "scalatest"                % "3.0.8"          % Test
    )
  )
